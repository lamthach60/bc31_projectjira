import logo from "./logo.svg";
import "./App.css";
import { Fragment } from "react";
import { LoginTemplate } from "./template/LoginTemplate";
import LoginPage from "./page/LoginPage";
import { BrowserRouter, Route, Router, Routes } from "react-router-dom";
import HomePage from "./page/Home/HomePage";
import CreateProject from "./component/CreateProject";
import Dashboard from "./component/Dashboard";
import ProjectMangement from "./page/ProjectMangement/ProjectMangement";
import { ModelJira } from "./HOC/ModelJira";

function App() {
  return (
    <Fragment>
      <ModelJira />
      <BrowserRouter>
        <Routes>
          <Route exact path="/" element={<LoginPage />} />
          <Route exact path="/home" element={<HomePage />}>
            <Route path="dashboard" element={<Dashboard />}></Route>
            <Route path="create" element={<CreateProject />}></Route>
            <Route
              exact
              path="projectmanagement"
              element={<ProjectMangement />}
            />
            <Route
              exact
              path="projectdetail/:projectId"
              element={<Dashboard />}
            />
          </Route>
        </Routes>
      </BrowserRouter>
    </Fragment>
  );
}

export default App;
