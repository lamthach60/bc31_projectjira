export const GET_ALL_TASK = "GET_ALL_TASK";
export const GET_ALL_TASK_SAGA = "GET_ALL_TASK_SAGA";
export const CREATE_TASK = "CREATE_TASK";
export const GET_TASK_SAGA = "GET_TASK_SAGA";
export const GET_TASK = "GET_TASK";
export const GET_TASK_DETAIL = "GET_TASK_DETAIL";
