import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import createMiddleWareSaga from "redux-saga";
import { ProjectCategoryReducer } from "./reducer/ProjectReducer";
import { rootSaga } from "./saga/rootSaga";
import { UserReducer } from "./reducer/UserReducer";
import { ProjectCyberBugsReducer } from "./reducer/ProjectCyberBugsReducer";
import { DrawerReducer } from "./reducer/DrawerReducer";
import { Project } from "./reducer/Project";
import { UserSearchReducer } from "./reducer/UserSearchReducer";
import { TaskTypeReducer } from "./reducer/TaskTypeReducer";
import { PriorityReducer } from "./reducer/PriorityReducer";
import { StatusReducer } from "./reducer/StatusReducer";
const MiddleWareSaga = createMiddleWareSaga();
const rootReducer = combineReducers({
  ProjectCategoryReducer,
  UserReducer,
  ProjectCyberBugsReducer,
  DrawerReducer,
  Project,
  UserSearchReducer,
  TaskTypeReducer,
  PriorityReducer,
  StatusReducer,
});
export const store = createStore(
  rootReducer,
  applyMiddleware(thunk, MiddleWareSaga)
);
MiddleWareSaga.run(rootSaga);
