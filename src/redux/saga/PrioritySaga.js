import {
  call,
  delay,
  put,
  takeEvery,
  takeLatest,
  select,
} from "redux-saga/effects";
import { PriorityService } from "../../service/PriorityService";
import {
  GET_ALL_PRIORITY,
  GET_ALL_PRIORITY_SAGA,
} from "../constants/PriorityConstants";

function* getAllPrioritySaga(action) {
  try {
    const { data, status } = yield call(() => PriorityService.getAllPriority());
    yield put({
      type: GET_ALL_PRIORITY,
      arrPriority: data.content,
    });
  } catch (err) {}
}
export function* theoDoiGetAllPrioritySaga() {
  yield takeLatest(GET_ALL_PRIORITY_SAGA, getAllPrioritySaga);
}
