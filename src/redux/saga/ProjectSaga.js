import {
  call,
  delay,
  put,
  takeEvery,
  takeLatest,
  select,
} from "redux-saga/effects";
import { TOKEN } from "../constants/token";
import { projectService } from "../../service/ProjectService";
import { useNavigate } from "react-router-dom";
import { history } from "../../util/history";
import { message } from "antd";
import { ProjectSer, projectSer } from "../../service/ProjectSer";
import { UserService } from "../../service/UserService";
import {
  GET_ALL_PROJECT,
  PUT_PROJECT_DETAIL,
} from "../constants/ProjectConstants";
import {
  GET_USER_BY_PROJECT_ID,
  GET_USER_BY_PROJECT_ID_SAGA,
} from "../constants/UserConstants";

function* createProjectSaga(action) {
  try {
    //gọi api lấy dữ liệu vế
    const { data, status } = yield call(() =>
      projectService.createProjectAuthorization(action.newProject)
    );
    //gọi api thành công thì dispatch lên reducer thông qua put

    message.success(data.message);
    window.location.reload();
  } catch (err) {}
}
export function* theoDoiCreateProjectSaga() {
  yield takeLatest("CREATEPROJECTSAGA", createProjectSaga);
}
//saga dùng để getall project từ api
function* getListProjectSaga(action) {
  try {
    const { data, status } = yield call(() => projectService.getListProject());
    yield put({
      type: "GET_LIST_PROJECT",
      projectList: data.content,
    });
  } catch (err) {}
}
export function* theoDoiGetListProjectSaga() {
  yield takeLatest("GET_LIST_PROJECT_SAGA", getListProjectSaga);
}
function* updateProjectSaga(action) {
  // console.log("action2", action);
  // return;
  try {
    //gọi api lấy dữ liệu vế
    const { data, status } = yield call(() =>
      projectService.updateProject(action.projectUpdate)
    );
    //gọi api thành công thì dispatch lên reducer thông qua put

    // message.success(data);
    // window.location.reload();
    yield put({
      type: "CLOSE_DRAWER",
    });
  } catch (err) {}
}
export function* theoDoiUpdateProjectSaga() {
  yield takeLatest("UPDATEPROJECTSAGA", updateProjectSaga);
}
function* deleteProjectSaga(action) {
  // console.log("action2", action);
  // return;
  try {
    //gọi api lấy dữ liệu vế
    yield call(() => projectSer.deleteProject(action.idProject));

    //gọi api thành công thì dispatch lên reducer thông qua put

    window.location.reload();
  } catch (err) {}
}
export function* theoDoiDeleteProjectSaga() {
  yield takeLatest("DELETEPROJECTSAGA", deleteProjectSaga);
}
function* getUserSaga(action) {
  // console.log("action2", action);
  // return;
  try {
    //gọi api lấy dữ liệu vế
    const { data, status } = yield call(() =>
      UserService.getUser(action.keyword)
    );

    yield put({
      type: "GET_USER_SEARCH",
      lstUserSearch: data.content,
    });
    //gọi api thành công thì dispatch lên reducer thông qua put
  } catch (err) {}
}
export function* theoDoiGetUser() {
  yield takeLatest("GETUSERAPI", getUserSaga);
}
function* addUserSaga(action) {
  // console.log("action2", action);
  // return;
  try {
    //gọi api lấy dữ liệu vế
    const { data, status } = yield call(() =>
      UserService.assignUser(action.userProject)
    );
    yield put({
      type: "GET_LIST_PROJECT_SAGA",
    });
    //gọi api thành công thì dispatch lên reducer thông qua put
  } catch (err) {}
}
export function* theoDoiAddUserSaga() {
  yield takeLatest("ADD_USER_PROJECT_API", addUserSaga);
}

function* removeUserProjectSaga(action) {
  // console.log("action2", action);
  // return;
  try {
    //gọi api lấy dữ liệu vế
    const { data, status } = yield call(() =>
      UserService.deleteUserFromProject(action.userProject)
    );
    yield put({
      type: "GET_LIST_PROJECT_SAGA",
    });
    //gọi api thành công thì dispatch lên reducer thông qua put
  } catch (err) {}
}
export function* theoDoiRemoveUserProjectSaga() {
  yield takeLatest("REMOVE_USER_PROJECT_API", removeUserProjectSaga);
}

function* getProjectDetailSaga(action) {
  try {
    //gọi api lấy dữ liệu vế
    const { data, status } = yield call(() =>
      projectService.getProjectDetail(action.projectId)
    );

    yield put({
      type: PUT_PROJECT_DETAIL,
      projectDetail: data.content,
    });

    //gọi api thành công thì dispatch lên reducer thông qua put
  } catch (err) {}
}
export function* theoDoiGetProjectDetailSaga() {
  yield takeLatest("GET_PROJECT_DETAIL", getProjectDetailSaga);
}

function* getAllProjectSaga(action) {
  try {
    //gọi api lấy dữ liệu vế
    const { data, status } = yield call(() => projectService.getAllProject());

    yield put({
      type: GET_ALL_PROJECT,
      arrProject: data.content,
    });

    //gọi api thành công thì dispatch lên reducer thông qua put
  } catch (err) {}
}
export function* theoDoiGetAllProjectSaga() {
  yield takeLatest("GET_ALL_PROJECT_SAGA", getAllProjectSaga);
}
function* getUserByProjectSaga(action) {
  const idProject = action.idProject;

  try {
    //gọi api lấy dữ liệu vế
    const { data, status } = yield call(() =>
      UserService.getUserByProjectId(idProject)
    );

    yield put({
      type: GET_USER_BY_PROJECT_ID,
      arrUser: data.content,
    });

    //gọi api thành công thì dispatch lên reducer thông qua put
  } catch (err) {
    message.error(err.response?.data.message);
  }
}
export function* theoDoiGetUserByProjectSaga() {
  yield takeLatest(GET_USER_BY_PROJECT_ID_SAGA, getUserByProjectSaga);
}
