import { all } from "redux-saga/effects";
import * as ProjectCategory from "./ProjectCategorySaga";
import * as ProjectSaga from "./ProjectSaga";
import * as TaskSaga from "./TaskSaga";
import * as PrioritySaga from "./PrioritySaga";
import * as StatusSaga from "./StatusSaga";
export function* rootSaga() {
  yield all([
    ProjectCategory.theoDoigetAllProjectCategory(),
    ProjectSaga.theoDoiCreateProjectSaga(),
    ProjectSaga.theoDoiGetListProjectSaga(),
    ProjectSaga.theoDoiUpdateProjectSaga(),
    ProjectSaga.theoDoiDeleteProjectSaga(),
    ProjectSaga.theoDoiGetUser(),
    ProjectSaga.theoDoiAddUserSaga(),
    ProjectSaga.theoDoiRemoveUserProjectSaga(),
    ProjectSaga.theoDoiGetProjectDetailSaga(),
    ProjectSaga.theoDoiGetAllProjectSaga(),
    TaskSaga.theoDoiGetAllTaskTypeSaga(),
    TaskSaga.theoDoiCreateTaskSaga(),
    TaskSaga.theoDoiGetTaskDetailSaga(),
    PrioritySaga.theoDoiGetAllPrioritySaga(),
    StatusSaga.theoDoiGetAllStatusSaga(),
    ProjectSaga.theoDoiGetUserByProjectSaga(),
  ]);
}
