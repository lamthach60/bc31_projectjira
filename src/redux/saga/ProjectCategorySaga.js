import {
  call,
  delay,
  put,
  takeEvery,
  takeLatest,
  select,
} from "redux-saga/effects";
import { projectService } from "../../service/ProjectService";
function* getAllProjectCategory(action) {
  try {
    //gọi api lấy dữ liệu vế
    const { data, status } = yield call(() =>
      projectService.getAllProjectCategory()
    );
    //gọi api thành công thì dispatch lên reducer thông qua put
    yield put({
      type: "GET_ALL_PROJECT_CATEGORY",
      data: data.content,
    });
  } catch (err) {}
}
export function* theoDoigetAllProjectCategory() {
  yield takeLatest("GETALLPROJECTSAGA", getAllProjectCategory);
}
