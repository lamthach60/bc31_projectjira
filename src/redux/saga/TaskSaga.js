import { message } from "antd";
import {
  call,
  delay,
  put,
  takeEvery,
  takeLatest,
  select,
} from "redux-saga/effects";
import { TaskService } from "../../service/TaskService";
import {
  CREATE_TASK,
  GET_ALL_TASK,
  GET_ALL_TASK_SAGA,
  GET_TASK,
  GET_TASK_DETAIL,
  GET_TASK_SAGA,
} from "../constants/TaskConstants";
function* getAllTaskTypeSaga(action) {
  try {
    const { data, status } = yield call(() => TaskService.getAllTaskType());
    yield put({
      type: GET_ALL_TASK,
      arrTaskType: data.content,
    });
  } catch (err) {}
}
export function* theoDoiGetAllTaskTypeSaga() {
  yield takeLatest(GET_ALL_TASK_SAGA, getAllTaskTypeSaga);
}
function* createTask(action) {
  try {
    const { data, status } = yield call(() =>
      TaskService.createTask(action.newTask)
    );
    yield put({
      type: GET_TASK,
      taskDetailModal: data.content,
    });
    console.log("data :", data);
    message.success(data.message);
    // window.location.reload();
  } catch (err) {}
}
export function* theoDoiCreateTaskSaga() {
  yield takeLatest(CREATE_TASK, createTask);
}
function* getTaskDetailSaga(action) {
  const { taskId } = action;
  try {
    const { data, status } = yield call(() =>
      TaskService.getTaskDetail(taskId)
    );
    yield put({
      type: GET_TASK_DETAIL,
      taskDetailModal: data.content,
    });
    console.log("data :", data);
    message.success(data.message);
    // window.location.reload();
  } catch (err) {}
}
export function* theoDoiGetTaskDetailSaga() {
  yield takeLatest(GET_TASK_SAGA, getTaskDetailSaga);
}
