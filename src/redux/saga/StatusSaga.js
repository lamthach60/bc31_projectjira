import { call, put, takeLatest } from "redux-saga/effects";

import { StatusService } from "../../service/StatusService";
import {
  GET_ALL_STATUS,
  GET_ALL_STATUS_SAGA,
} from "../constants/StatusConstants";

function* getAllStatusSaga(action) {
  try {
    const { data, status } = yield call(() => StatusService.getAllStatus());
    yield put({
      type: GET_ALL_STATUS,
      arrStatus: data.content,
    });
  } catch (err) {}
}
export function* theoDoiGetAllStatusSaga() {
  yield takeLatest(GET_ALL_STATUS_SAGA, getAllStatusSaga);
}
