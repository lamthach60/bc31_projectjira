import { GET_ALL_TASK, GET_TASK_DETAIL } from "../constants/TaskConstants";

const initialState = {
  arrTaskType: [],
  taskDetailModal: {},
};

export const TaskTypeReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_TASK: {
      return { ...state, arrTaskType: action.arrTaskType };
    }
    case GET_TASK_DETAIL: {
      return { ...state, taskDetailModal: action.taskDetailModal };
    }
    default:
      return state;
  }
};
