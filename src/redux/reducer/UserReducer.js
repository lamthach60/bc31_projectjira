import { DANGNHAP, TOKEN, USERLOGIN } from "../constants/token";

let user = {};
if (localStorage.getItem(USERLOGIN)) {
  user = JSON.parse(localStorage.getItem(USERLOGIN));
}
const initialState = {
  userLogin: user,
};

export const UserReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case DANGNHAP:
      state.userLogin = payload;
      localStorage.setItem(USERLOGIN, JSON.stringify(payload));
      localStorage.setItem(TOKEN, payload.accessToken);
      return { ...state, userLogin: payload };

    default:
      return state;
  }
};
