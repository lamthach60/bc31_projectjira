import { GET_USER_SEARCH } from "../constants/token";
import { GET_USER_BY_PROJECT_ID } from "../constants/UserConstants";

const initialState = {
  userSearch: [],
  arrUser: [],
};

export const UserSearchReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USER_SEARCH: {
      state.userSearch = action.lstUserSearch;
      return { ...state };
    }
    case GET_USER_BY_PROJECT_ID: {
      state.arrUser = action.arrUser;
      return { ...state };
    }
    default:
      return state;
  }
};
