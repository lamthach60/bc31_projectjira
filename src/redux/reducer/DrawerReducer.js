import { message } from "antd";
import React from "react";

const initialState = {
  setOpen: false,
  ComponentContentDrawer: () => {
    return <p>default</p>;
  },
  callBackSubmit: (propsValue) => {
    console.log(propsValue);
  },
  title: "",
};

export const DrawerReducer = (state = initialState, action) => {
  switch (action.type) {
    case "OPEN_DRAWER":
      return { ...state, setOpen: true };
    case "CLOSE_DRAWER":
      return { ...state, setOpen: false };
    case "OPEN_EDIT_FORM_PROJECT":
      state.setOpen = true;
      state.ComponentContentDrawer = action.ComponentContentDrawer;
      state.title = action.title;
      return {
        ...state,
      };
    case "SET_SUBMIT_EDIT_PROJECT": {
      state.callBackSubmit = action.submitFunction;
      return { ...state };
    }
    case "OPEN_FORM_CREATE_TASK": {
      state.setOpen = true;
      state.title = action.title;
      state.ComponentContentDrawer = action.ComponentContentDrawer;
      return { ...state };
    }
    case "SET_SUBMIT_CREATE_TASK": {
      return { ...state, callBackSubmit: action.submitFunction };
    }
    default:
      return state;
  }
};
