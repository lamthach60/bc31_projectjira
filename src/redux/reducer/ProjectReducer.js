import { GET_ALL_PROJECT_CATEGORY } from "../constants/ProjectConstants";

const stateDefault = {
  arrProjectCategory: [],
};

export const ProjectCategoryReducer = (state = stateDefault, action) => {
  switch (action.type) {
    case GET_ALL_PROJECT_CATEGORY: {
      state.arrProjectCategory = action.data;
    }
    default:
      return { ...state };
  }
};
