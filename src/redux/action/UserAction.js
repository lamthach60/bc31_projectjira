import { message } from "antd";
import { useNavigate } from "react-router-dom";
import { UserService } from "../../service/UserService";
import { DANGNHAP } from "../constants/token";

export const DangNhapAction = (thongTinNguoiDung) => {
  return (dispatch) => {
    UserService.dangNhap(thongTinNguoiDung)
      .then((result) => {
        console.log("result :", result);
        dispatch({
          type: DANGNHAP,
          payload: result.data.content,
        });
        message.success(result.data.content);
      })
      .catch((err) => {
        console.log("err :", err);
      });
  };
};
