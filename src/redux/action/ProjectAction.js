import { projectService } from "../../service/ProjectService";

export const ProjectAction = () => {
  return (dispatch) => {
    projectService
      .getAllProjectCategory()
      .then((result) => {
        console.log("result :", result);
        dispatch({
          type: "ADDPROJECT",
          payload: result.data.content,
        });
      })

      .catch((err) => {
        console.log("err :", err);
      });
  };
};
