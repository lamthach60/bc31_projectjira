import axios from "axios";
import { TOKENCYBERSOFT } from "../redux/constants/token";
import { https } from "./configService";
export const TaskService = {
  getAllTaskType: () => {
    return axios({
      url: `https://jiranew.cybersoft.edu.vn/api/TaskType/getAll`,
      method: "GET",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
        TokenCybersoft: TOKENCYBERSOFT,
      },
    });
  },
  createTask: (newTask) => {
    return https.post("/api/Project/createTask", newTask);
  },
  getTaskDetail: (taskId) => {
    return https.get(`/api/Project/getTaskDetail?taskId=${taskId}`);
  },
};
