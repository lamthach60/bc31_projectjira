import axios from "axios";
import { TOKEN, TOKENCYBERSOFT } from "../redux/constants/token";
import { https } from "./configService";

export const projectService = {
  getAllProjectCategory: () => {
    return https.get("/api/ProjectCategory");
  },
  createProject: (newProject) => {
    return https.post("/api/Project/createProject", newProject);
  },
  createProjectAuthorization: (newProject) => {
    return axios({
      url: "https://jiranew.cybersoft.edu.vn/api/Project/createProjectAuthorize",
      method: "POST",
      data: newProject,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
        TokenCybersoft: TOKENCYBERSOFT,
      },
    });
  },
  getListProject: () => {
    return axios({
      url: "https://jiranew.cybersoft.edu.vn/api/Project/getAllProject",
      method: "GET",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
        TokenCybersoft: TOKENCYBERSOFT,
      },
    });
  },
  updateProject: (projectUpdate) => {
    return axios({
      url: `https://jiranew.cybersoft.edu.vn/api/Project/updateProject?projectId=${projectUpdate.id}`,
      method: "PUT",
      data: projectUpdate,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
        TokenCybersoft: TOKENCYBERSOFT,
      },
    });
  },
  getProjectDetail: (projectId) => {
    return axios({
      url: `https://jiranew.cybersoft.edu.vn/api/Project/getProjectDetail?id=${projectId}`,
      method: "GET",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
        TokenCybersoft: TOKENCYBERSOFT,
      },
    });
  },
  getAllProject: () => {
    return axios({
      url: "https://jiranew.cybersoft.edu.vn/api/Project/getAllProject",
      method: "GET",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
        TokenCybersoft: TOKENCYBERSOFT,
      },
    });
  },
};
