import axios from "axios";
import { TOKENCYBERSOFT } from "../redux/constants/token";
import { https } from "./configService";
export const PriorityService = {
  getAllPriority: () => {
    return axios({
      url: `https://jiranew.cybersoft.edu.vn/api/Priority/getAll`,
      method: "GET",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
        TokenCybersoft: TOKENCYBERSOFT,
      },
    });
  },
};
