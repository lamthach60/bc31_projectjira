import axios from "axios";
import { TOKENCYBERSOFT } from "../redux/constants/token";

export class baseService {
  put = (url, id, model) => {
    return axios({
      url: `https://jiranew.cybersoft.edu.vn/${url}`,
      method: "PUT",
      data: model,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
        TokenCybersoft: TOKENCYBERSOFT,
      },
    });
  };
  post = (url, model) => {
    return axios({
      url: `https://jiranew.cybersoft.edu.vn/${url}`,
      method: "POST",
      data: model,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
        TokenCybersoft: TOKENCYBERSOFT,
      },
    });
  };
  get = (url, model) => {
    return axios({
      url: `https://jiranew.cybersoft.edu.vn/${url}`,
      method: "GET",
      data: model,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
        TokenCybersoft: TOKENCYBERSOFT,
      },
    });
  };
  delete = (url) => {
    return axios({
      url: `https://jiranew.cybersoft.edu.vn/api/${url}`,
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
        TokenCybersoft: TOKENCYBERSOFT,
      },
    });
  };
}
