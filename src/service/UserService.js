import axios from "axios";
import { TOKENCYBERSOFT } from "../redux/constants/token";
import { https } from "./configService";

export const UserService = {
  dangNhap: (thongTinNguoiDung) => {
    return https.post("/api/Users/signin", thongTinNguoiDung);
  },
  getUser: (keyword) => {
    return axios({
      url: `https://jiranew.cybersoft.edu.vn/api/Users/getUser?keyword=${keyword}`,
      method: "GET",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
        TokenCybersoft: TOKENCYBERSOFT,
      },
    });
  },
  assignUser: (userProject) => {
    return https.post("/api/Project/assignUserProject", userProject);
  },
  deleteUserFromProject: (userProject) => {
    return axios({
      url: `https://jiranew.cybersoft.edu.vn/api/Project/removeUserFromProject`,
      method: "POST",
      data: userProject,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
        TokenCybersoft: TOKENCYBERSOFT,
      },
    });
  },
  getUserByProjectId: (idProject) => {
    return axios({
      url: `https://jiranew.cybersoft.edu.vn/api/Users/getUserByProjectId?idProject=${idProject}`,
      method: "GET",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
        TokenCybersoft: TOKENCYBERSOFT,
      },
    });
  },
};
