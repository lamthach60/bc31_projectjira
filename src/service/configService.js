import axios from "axios";
import { TOKEN, TOKENCYBERSOFT } from "../redux/constants/token";

export let https = axios.create({
  baseURL: "https://jiranew.cybersoft.edu.vn",
  headers: {
    TokenCybersoft: TOKENCYBERSOFT,
    Authorization: "Bearer " + TOKEN,
  },
});
