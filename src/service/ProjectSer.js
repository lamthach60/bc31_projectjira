import { baseService } from "./baseService";

export class ProjectSer extends baseService {
  constructor() {
    super();
  }
  deleteProject = (id) => {
    return this.delete(`/Project/deleteProject?projectId=${id}`);
  };
}
export const projectSer = new ProjectSer();
