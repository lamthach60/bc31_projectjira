import React, { useState } from "react";
import { Layout, Menu } from "antd";
import {
  MenuFoldOutlined,
  BarsOutlined,
  UploadOutlined,
  PlusOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import "../index.css";
import { useDispatch, useSelector } from "react-redux";
import { Component } from "react";
import FormCreateTask from "./Form/FormCreateTask";
const { Header, Sider, Content } = Layout;
export function SideBar() {
  const [state, setState] = useState({
    collapsed: false,
  });
  const toggle = () => {
    setState({
      collapsed: !state.collapsed,
    });
  };

  const dispatch = useDispatch();
  return (
    <>
      <div>
        <Sider
          trigger={null}
          collapsible
          collapsed={state.collapsed}
          style={{ height: "110%" }}
        >
          <div className="text-right mr-2" onClick={toggle}>
            <BarsOutlined
              style={{ cursor: "pointer", color: "white", fontSize: "20px" }}
            />
          </div>
          <div className="logo" />
          <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
            <Menu.Item
              key="1"
              icon={<PlusOutlined />}
              onClick={() => {
                dispatch({
                  type: "OPEN_FORM_CREATE_TASK",
                  ComponentContentDrawer: <FormCreateTask />,
                });
              }}
            >
              Create Task
            </Menu.Item>
          </Menu>
        </Sider>
      </div>
    </>
  );
}
