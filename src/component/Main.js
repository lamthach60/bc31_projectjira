import { render } from "@testing-library/react";
import React from "react";
import "../index.css";
import parse from "html-react-parser";
import { useDispatch } from "react-redux";
import { GET_TASK_SAGA } from "../redux/constants/TaskConstants";
export function Main(props) {
  const { projectDetail } = props;
  const dispatch = useDispatch();
  const renderAvatar = () => {
    return projectDetail.members?.map((user, index) => {
      return (
        <div key={index} className="avatar">
          <img src={user.avatar} alt="" />
        </div>
      );
    });
  };
  const renderCardTaskList = () => {
    return projectDetail.lstTask?.map((taskListDetail, index) => {
      return (
        <div
          key={index}
          className="card pb-2"
          style={{ width: "17rem", height: "auto" }}
        >
          <div className="card-header">{taskListDetail.statusName}</div>
          <ul className="list-group list-group-flush">
            {taskListDetail.lstTaskDeTail.map((task, index) => {
              return (
                <li
                  key={index}
                  className="list-group-item"
                  data-toggle="modal"
                  data-target="#infoModal"
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                    dispatch({
                      type: GET_TASK_SAGA,
                      taskId: task.taskId,
                    });
                  }}
                >
                  <p className="font-bold">{task.taskName}</p>
                  <div className="block" style={{ display: "flex" }}>
                    <div className="block-left">
                      <p className="text-danger">
                        {task.priorityTask.priority}
                      </p>
                    </div>
                    <div className="block-right">
                      <div className="avatar-group" style={{ display: "flex" }}>
                        {task.assigness.map((mem, index) => {
                          return (
                            <div className="avatar" key={index}>
                              <img src={mem.avatar} alt />
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>
      );
    });
  };
  return (
    <div className="main">
      <div className="info pt-5" style={{ display: "flex" }}>
        <div className="search-block">
          <input className="search" />
          <i className="fa fa-search" />
        </div>
        <div className="avatar-group" style={{ display: "flex" }}>
          {renderAvatar()}
        </div>
        <div style={{ marginLeft: 20 }} className="text">
          Only My Issues
        </div>
        <div style={{ marginLeft: 20 }} className="text">
          Recently Updated
        </div>
      </div>
      <h3 className="text-4xl">{projectDetail.projectName}</h3>
      {/* <p className="text-2xl">{projectDetail.description}></p> */}
      <div className="content" style={{ display: "flex" }}>
        {renderCardTaskList()}
      </div>
    </div>
  );
}
