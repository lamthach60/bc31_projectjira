import React from "react";
import { NavLink } from "react-router-dom";
import "../index.css";
export function MenuJira() {
  return (
    <>
      <div className="menu">
        <div className="account">
          <div className="avatar">
            <img src={require("../assets/img/download (1).jfif")} />
          </div>
          <div className="account-info">
            <p>CyberLearn.vn</p>
            <p>Report bugs</p>
          </div>
        </div>
        <div className="control">
          <div>
            <i className="fa fa-credit-card mr-2" />
            <NavLink className="text-black" to="/home/dashboard">
              Dashboard
            </NavLink>
          </div>
          <div>
            <i className="fa fa-cog mr-2" />
            <NavLink className="text-black" to="/home/create">
              Create Project
            </NavLink>
          </div>
          <div>
            <i className="fa fa-cog mr-2" />
            <NavLink className="text-black" to="/home/projectmanagement">
              Project
            </NavLink>
          </div>
        </div>
        <div className="feature">
          <div>
            <i className="fa fa-truck mr-2" />
            <span>Releases</span>
          </div>
          <div>
            <i className="fa fa-equals mr-2" />
            <span>Issues and filters</span>
          </div>
          <div>
            <i className="fa fa-paste mr-2" />
            <span>Pages</span>
          </div>
          <div>
            <i className="fa fa-location-arrow mr-2" />
            <span>Reports</span>
          </div>
          <div>
            <i className="fa fa-box mr-2" />
            <span>Components</span>
          </div>
        </div>
      </div>
    </>
  );
}
