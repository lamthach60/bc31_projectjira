import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import InfoMain from "./InfoMain";
import { Main } from "./Main";

export default function Dashboard(props) {
  let { projectDetail } = useSelector((state) => state.Project);

  let dispatch = useDispatch();
  const { projectId } = useParams();
  useEffect(() => {
    dispatch({
      type: "GET_PROJECT_DETAIL",
      projectId,
    });
  }, []);

  return (
    <>
      <Main projectDetail={projectDetail} />
      <InfoMain projectDetail={projectDetail} />
    </>
  );
}
