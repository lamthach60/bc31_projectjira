import React from "react";
import { Editor } from "@tinymce/tinymce-react";
import { Radio, Select } from "antd";
import { useState } from "react";
import { Slider } from "antd";
import { connect, useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import {
  CREATE_TASK,
  GET_ALL_TASK_SAGA,
} from "../../redux/constants/TaskConstants";
import { GET_ALL_PRIORITY_SAGA } from "../../redux/constants/PriorityConstants";
import { GET_ALL_STATUS_SAGA } from "../../redux/constants/StatusConstants";
import { withFormik } from "formik";
import { GET_USER_BY_PROJECT_ID_SAGA } from "../../redux/constants/UserConstants";
export function FormCreateTask(props) {
  const {
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
    setFieldValue,
  } = props;
  const { arrProject } = useSelector((state) => state.ProjectCyberBugsReducer);
  const { arrTaskType } = useSelector((state) => state.TaskTypeReducer);
  const { arrPriority } = useSelector((state) => state.PriorityReducer);
  const { userSearch } = useSelector((state) => state.UserSearchReducer);
  const { arrUser } = useSelector((state) => state.UserSearchReducer);

  const { arrStatus } = useSelector((state) => state.StatusReducer);

  const userOptions = arrUser.map((item, index) => {
    return { value: item.userId, label: item.name };
  });

  const dispatch = useDispatch();
  const { Option } = Select;
  const handleEditChange = (content, editor) => {};
  const children = [];
  const [timeTracking, setTimeTracking] = useState({
    timeTrackingSpent: 0,
    timeTrackingRemaining: 0,
  });
  for (let i = 10; i < 36; i++) {
    children.push(
      <Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>
    );
  }

  const [size, setSize] = useState("middle");
  useEffect(() => {
    dispatch({ type: "GET_ALL_PROJECT_SAGA" });
    dispatch({ type: GET_ALL_TASK_SAGA });
    dispatch({ type: GET_ALL_PRIORITY_SAGA });
    dispatch({ type: GET_ALL_STATUS_SAGA });
    dispatch({
      type: "SET_SUBMIT_CREATE_TASK",
      submitFunction: handleSubmit,
    });
  }, []);

  return (
    <div className="container">
      <form className="container-fluid" onSubmit={handleSubmit}>
        <div className="form-group">
          <p>Project</p>
          <select
            name="projectId"
            className="form-control"
            onChange={(e) => {
              let { value } = e.target;
              dispatch({
                type: GET_USER_BY_PROJECT_ID_SAGA,
                idProject: value,
              });
              setFieldValue("projectId", e.target.value);
            }}
          >
            {arrProject.map((project, index) => {
              return (
                <option key={index} value={project.id}>
                  {project.projectName}
                </option>
              );
            })}
          </select>
        </div>
        <div className="form-group">
          <p>Task Name:</p>
          <input
            name="taskName"
            className="form-control"
            onChange={handleChange}
          ></input>
        </div>
        <div className="form-group">
          <p>Status:</p>
          <select
            name="statusId"
            className="form-control"
            onChange={handleChange}
          >
            {arrStatus.map((statusItem, index) => {
              return (
                <option key={index} value={statusItem.statusId}>
                  {statusItem.statusName}
                </option>
              );
            })}
          </select>
        </div>
        <div className="form-group">
          <div className="row">
            <div className="col-6">
              <p>Priority:</p>
              <select name="priorityId" className="form-control">
                {arrPriority.map((priority, index) => {
                  return (
                    <option key={index} value={priority.priorityId}>
                      {priority.priority}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="col-6">
              <p>Task Type:</p>
              <select className="form-control" name="typeId">
                {arrTaskType.map((task, index) => {
                  return (
                    <option key={index} value={task.id}>
                      {task.taskType}
                    </option>
                  );
                })}
              </select>
            </div>
          </div>
        </div>
        <div className="form-group">
          <div className="row">
            <div className="col-6">
              <p>Assign:</p>
              <Select
                options={userOptions}
                mode="multiple"
                size={size}
                placeholder="Please select"
                // defaultValue={["a10", "c12"]}
                optionFilterProp="label"
                onChange={(values) => {
                  setFieldValue("listUserAsign", values);
                }}
                style={{
                  width: "100%",
                }}
                onSearch={(value) => {
                  console.log(value);
                  dispatch({
                    type: "GETUSERAPI",
                    keyword: "",
                  });
                }}
              >
                {children}
              </Select>
              <div className="row mt-9">
                <div className="col-12">
                  <p>Original Estimate:</p>
                  <input
                    type="number"
                    min="0"
                    name="originalEstimate"
                    className="form-control"
                    defaultValue="0"
                  ></input>
                </div>
              </div>
            </div>
            <div className="col-6 mb-1">
              <p>Time Tracking:</p>
              <Slider
                range={{
                  draggableTrack: true,
                }}
                defaultValue={30}
                value={timeTracking.timeTrackingSpent}
                max={Number(
                  timeTracking.timeTrackingSpent +
                    Number(timeTracking.timeTrackingRemaining)
                )}
              />
              <div className="row">
                <div className="col-6 text-left font-bold">
                  {timeTracking.timeTrackingSpent}h logged
                </div>
                <div className="col-6 text-right font-bold">
                  {timeTracking.timeTrackingRemaining}h remaining
                </div>
              </div>
              <div className="row mt-3">
                <div className="col-6">
                  <p>Time spent</p>
                  <input
                    type="number"
                    defaultValue="0"
                    className="form-control"
                    min="0"
                    onChange={(e) => {
                      setTimeTracking({
                        ...timeTracking,
                        timeTrackingSpent: e.target.value,
                      });
                    }}
                  ></input>
                </div>
                <div className="col-6">
                  <p>Time remaining</p>
                  <input
                    type="number"
                    defaultValue="0"
                    min="0"
                    className="form-control"
                    name="timeTrackingRemaining"
                    onChange={(e) => {
                      setTimeTracking({
                        ...timeTracking,
                        timeTrackingRemaining: e.target.value,
                      });
                    }}
                  ></input>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="form-group">
          <p>Description:</p>
          <Editor
            name="description"
            // value={values.description}
            init={{
              height: 500,
              menubar: false,
              plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table paste code help wordcount",
              ],
              toolbar:
                "undo redo | formatselect | " +
                "bold italic backcolor | alignleft aligncenter " +
                "alignright alignjustify | bullist numlist outdent indent | " +
                "removeformat | help",
              content_style:
                "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
            }}
            onEditorChange={handleEditChange}
          />
        </div>
      </form>
    </div>
  );
}
const editProjectFrm = withFormik({
  enableReinitialize: true,
  mapPropsToValues: (props) => {
    const { arrProject, arrTaskType, arrPriority, userSearch, arrStatus } =
      props;
    if (arrProject.length > 0) {
      props.dispatch({
        type: GET_USER_BY_PROJECT_ID_SAGA,
        idProject: arrProject[0]?.id,
      });
    }
    return {
      taskName: "",
      description: "",
      statusId: arrStatus[0]?.statusId,
      originalEstimate: 0,
      timeTrackingSpent: 0,
      timeTrackingRemaining: 0,
      projectId: arrProject[0]?.id,
      typeId: arrTaskType[0]?.id,
      priorityId: arrPriority[0]?.priorityId,
      listUserAsign: [],
    };
  },
  handleSubmit: (values, { props, setSubmitting }) => {
    console.log("values", values);
    props.dispatch({
      type: CREATE_TASK,
      newTask: values,
    });
  },

  displayName: "EditProjectForm",
})(FormCreateTask);
// const { arrProject } = useSelector((state) => state.ProjectCyberBugsReducer);
// const { arrTaskType } = useSelector((state) => state.TaskTypeReducer);
// const { arrPriority } = useSelector((state) => state.PriorityReducer);
// const { userSearch } = useSelector((state) => state.UserSearchReducer);
// const { arrStatus } = useSelector((state) => state.StatusReducer);
const mapStateToProps = (state) => {
  return {
    arrProject: state.ProjectCyberBugsReducer.arrProject,
    arrTaskType: state.TaskTypeReducer.arrTaskType,
    arrPriority: state.PriorityReducer.arrPriority,
    userSearch: state.UserSearchReducer.userSearch,
    arrStatus: state.StatusReducer.arrStatus,
  };
};
export default connect(mapStateToProps)(editProjectFrm);
