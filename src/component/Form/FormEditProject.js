import React, { useEffect, useRef, useState } from "react";
import { Editor } from "@tinymce/tinymce-react";
import { connect, useDispatch, useSelector } from "react-redux";
import { withFormik } from "formik";
export function FormEditProject(props) {
  const [state, setState] = useState();
  const {
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
    setFieldValue,
  } = props;
  const arrProjectCategory = useSelector(
    (state) => state.ProjectCategoryReducer.arrProjectCategory
  );
  const editorRef = useRef(null);
  const handleEditorChange = (content, editor) => {
    setFieldValue("description", content);
  };
  const dispatch = useDispatch();
  const submitForm = (e) => {
    e.preventDefault();
  };
  useEffect(() => {
    setFieldValue("description", values.description);
    dispatch({ type: "GETALLPROJECTSAGA" });
    function Dispatch() {
      dispatch({
        type: "SET_SUBMIT_EDIT_PROJECT",
        submitFunction: handleSubmit,
      });
    }
    Dispatch();
  }, []);
  return (
    <div>
      <form className="container-fluid" onSubmit={handleSubmit}>
        <div className="row">
          <div className="col-4">
            <div className="form-group">
              <h3 className="font-weight-bold">Project ID</h3>
              <input
                value={values.id}
                className="form-control"
                name="id"
                disabled
              ></input>
            </div>
          </div>
          <div className="col-4">
            <div className="form-group">
              <h3 className="font-weight-bold">Project Name</h3>
              <input
                value={values.projectName}
                className="form-control"
                name="projectName"
                onChange={handleChange}
              ></input>
            </div>
          </div>
          <div className="col-4">
            <div className="form-group">
              <h3 className="font-weight-bold">Project Category</h3>
              <select
                value={values.categoryId}
                className="form-control"
                name="categoryId"
              >
                {arrProjectCategory.map((item, index) => {
                  return (
                    <option key={index} value={item.id}>
                      {item.projectCategoryName}
                    </option>
                  );
                })}
              </select>
            </div>
          </div>
          <div className="col-12">
            <div className="form-group">
              <h3 className="font-weight-bold">Project Description</h3>
              <Editor
                name="description"
                onInit={(evt, editor) => (editorRef.current = editor)}
                initialValue={values.description}
                // value={values.description}
                init={{
                  height: 500,
                  menubar: false,
                  plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table paste code help wordcount",
                  ],
                  toolbar:
                    "undo redo | formatselect | " +
                    "bold italic backcolor | alignleft aligncenter " +
                    "alignright alignjustify | bullist numlist outdent indent | " +
                    "removeformat | help",
                  content_style:
                    "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
                }}
                onEditorChange={handleEditorChange}
              />
            </div>
          </div>
        </div>
      </form>
    </div>
  );
}
const editProjectForm = withFormik({
  enableReinitialize: true,
  mapPropsToValues: (props) => {
    const { projectEdit } = props;
    return {
      id: projectEdit?.id,
      projectName: projectEdit.projectName,
      description: projectEdit.description,
      categoryId: projectEdit.categoryId,
    };
  },
  handleSubmit: (values, { props, setSubmitting }) => {
    console.log("values", values);

    //gọi saga
    props.dispatch({
      type: "UPDATEPROJECTSAGA",
      projectUpdate: values,
    });
  },

  displayName: "EditProjectForm",
})(FormEditProject);
const mapStateToProps = (state) => {
  return {
    projectEdit: state.Project.projectEdit,
  };
};
export default connect(mapStateToProps)(editProjectForm);
