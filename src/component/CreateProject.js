import React, { useRef, useEffect } from "react";
import { Editor } from "@tinymce/tinymce-react";
import { withFormik, yupToFormErrors } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector, connect } from "react-redux";
import { useFormik } from "formik";
import { ProjectAction } from "../redux/action/ProjectAction";
function CreateProject(props) {
  const arrProjectCategory = useSelector(
    (state) => state.ProjectCategoryReducer.arrProjectCategory
  );
  console.log("arrProjectCategory :", arrProjectCategory);
  const {
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
    setFieldValue,
  } = props;
  const editorRef = useRef(null);
  const log = () => {
    if (editorRef.current) {
      console.log(editorRef.current.getContent());
    }
  };
  const handleEditorChange = (content, editor) => {
    setFieldValue("description", content);
  };
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch({ type: "GETALLPROJECTSAGA" });
  }, []);

  return (
    <div className="container m-5">
      <form className="container" onSubmit={handleSubmit}>
        <div className="form-group">
          <p>Name:</p>
          <input
            className="form-control"
            name="projectName"
            onChange={handleChange}
          />
        </div>
        <div className="form-group">
          <p>Description:</p>
          {/* <input className="form-control" name="description" /> */}
          <Editor
            name="description"
            onInit={(evt, editor) => (editorRef.current = editor)}
            initialValue="<p >This is the initial content of the editor.</p>"
            init={{
              height: 500,
              menubar: false,
              plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table paste code help wordcount",
              ],
              toolbar:
                "undo redo | formatselect | " +
                "bold italic backcolor | alignleft aligncenter " +
                "alignright alignjustify | bullist numlist outdent indent | " +
                "removeformat | help",
              content_style:
                "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
            }}
            onEditorChange={handleEditorChange}
          />
        </div>
        <div className="form-group">
          <select
            name="categoryId"
            className="form-control"
            onChange={handleChange}
          >
            {arrProjectCategory.map((item, index) => {
              return (
                <option value={item.id} key={index}>
                  {item.projectCategoryName}
                </option>
              );
            })}
          </select>
        </div>
        <button className="btn btn-outline-primary" type="submit">
          Create
        </button>
      </form>
    </div>
  );
}
const createProjectForm = withFormik({
  enableReinitialize: true,
  mapPropsToValues: (props) => {
    console.log("propvalue", props);
    return {
      projectName: "",
      description: "",
      categoryId: props.arrProjectCategory[0]?.id,
    };
  },
  handleSubmit: (values, { props, setSubmitting }) => {
    console.log("props :", values);
    props.dispatch({
      type: "CREATEPROJECTSAGA",
      newProject: values,
    });
  },

  displayName: "CreateProjectFormik",
})(CreateProject);
const mapStateToProps = (state) => {
  return {
    arrProjectCategory: state.ProjectCategoryReducer.arrProjectCategory,
  };
};
export default connect(mapStateToProps)(createProjectForm);
