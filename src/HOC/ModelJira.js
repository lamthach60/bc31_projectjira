import { PlusOutlined } from "@ant-design/icons";
import {
  Button,
  Col,
  DatePicker,
  Drawer,
  Form,
  Input,
  Row,
  Select,
  Space,
} from "antd";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

const { Option } = Select;
export function ModelJira(props) {
  const { setOpen, ComponentContentDrawer, callBackSubmit, title } =
    useSelector((state) => state.DrawerReducer);
  const dispatch = useDispatch();
  const [state, setState] = useState({
    setOpen: false,
  });
  const showDrawer = () => {
    dispatch({ type: "OPEN_DRAWER", setOpen: true });
  };
  const onClose = () => {
    dispatch({ type: "CLOSE_DRAWER", setOpen: false });
  };
  return (
    <>
      <Drawer
        title={title}
        width={720}
        open={setOpen}
        bodyStyle={{
          paddingBottom: 80,
        }}
        extra={
          <Space>
            <Button onClick={onClose}>Cancel</Button>
            <Button onClick={callBackSubmit} type="button">
              Submit
            </Button>
          </Space>
        }
      >
        {/* Nội dung thay đổi của Drawer */}

        {ComponentContentDrawer}
      </Drawer>
    </>
  );
}
