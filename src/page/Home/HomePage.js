import React from "react";
import { MenuJira } from "../../component/MenuJira";
import { SideBar } from "../../component/SideBar";
import { Main } from "../../component/Main";
import "../../index.css";
import InfoMain from "../../component/InfoMain";
import { Outlet } from "react-router-dom";
export default function HomePage() {
  return (
    <>
      <div className="jira">
        <SideBar></SideBar>
        <MenuJira></MenuJira>
        {/* <Main /> */}
        <Outlet />
      </div>
      {/* <InfoMain></InfoMain> */}
    </>
  );
}
