import {
  Button,
  Input,
  Space,
  Table,
  Tag,
  message,
  Popconfirm,
  Avatar,
  AutoComplete,
  Popover,
} from "antd";
import React, { useEffect, useRef, useState } from "react";
import { EditOutlined, DeleteOutlined, PlusOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import FormEditProject from "../../component/Form/FormEditProject";
import { retry } from "redux-saga/effects";
import { NavLink } from "react-router-dom";
import FormCreateTask from "../../component/Form/FormCreateTask";

const data = [
  {
    id: 8382,
    projectName: "string",
    description: "<p>string</p>",
    categoryId: 1,
    categoryName: "Dự án web",
    alias: "string",
    deleted: false,
  },
  {
    id: 8383,
    projectName: "xindungxoa",
    description: "dsadasd",
    categoryId: 2,
    categoryName: "Dự án phần mềm",
    alias: "xindungxoa",
    deleted: false,
  },
  {
    id: 8384,
    projectName: "xoahoaiz",
    description: "sdadad",
    categoryId: 1,
    categoryName: "Dự án web",
    alias: "xoahoaiz",
    deleted: false,
  },
];
const onChange = (pagination, filters, sorter, extra) => {
  // console.log("params", pagination, filters, sorter, extra);
};

const cancel = (e) => {
  console.log(e);
  message.error("Click on No");
};
export default function ProjectMangement() {
  const { userSearch } = useSelector((state) => state.UserSearchReducer);
  //lấy dữ liệu từ reducer về compo
  const projectList = useSelector(
    (state) => state.ProjectCyberBugsReducer.projectList
  );
  console.log("projectList :", projectList);
  const [value, setValue] = useState("");
  //sử dụng dispatch gọi action
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch({ type: "GET_LIST_PROJECT_SAGA" });
  }, []);
  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
      sorter: (item2, item1) => {
        return item2.id - item1.id;
      },

      // specify the condition of filtering result
      // here is that finding the name started with `value`
    },

    {
      title: "projectName",
      dataIndex: "projectName",
      key: "projectName",
      render: (text, record, index) => {
        return (
          <NavLink to={`/home/projectdetail/${record.id}`}>{text}</NavLink>
        );
      },
      sorter: (item2, item1) => {
        let projectName1 = item1.projectName?.trim().toLowerCase();
        let projectName2 = item2.projectName?.trim().toLowerCase();
        if (projectName2 < projectName1) {
          return -1;
        }
        return 1;
      },
    },

    {
      title: "description",
      dataIndex: "description",
      key: "description",

      render: (text, record, index) => {
        return <p>{text.replace(/<[^>]+>/g, "")}</p>;
      },
    },
    {
      title: "creator",
      // dataIndex: "creator",
      key: "creator",
      render: (text, record, index) => {
        return <Tag color="green">{record.creator?.name}</Tag>;
      },
      sorter: (item2, item1) => {
        let projectName1 = item1.creator.name?.trim().toLowerCase();
        let projectName2 = item2.creator.name?.trim().toLowerCase();
        if (projectName2 < projectName1) {
          return -1;
        }
        return 1;
      },
    },
    {
      title: "member",
      key: "member",
      render: (text, record, index) => {
        return (
          <div className="flex justify-center text-left ">
            {record.members?.slice(0, 2).map((member, index) => {
              return (
                <Popover
                  key={index}
                  placement="top"
                  title={"Member"}
                  content={() => {
                    return (
                      <table className="table">
                        <thead>
                          <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Avatar</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          {record.members?.map((item, index) => {
                            return (
                              <tr>
                                <td>{item.userId}</td>
                                <td>{item.name}</td>
                                <td>
                                  <img
                                    className="rounded-full"
                                    src={item.avatar}
                                    width="30"
                                    height="30"
                                  />
                                </td>
                                <td>
                                  <button
                                    className="btn btn-danger rounded-2xl"
                                    onClick={() => {
                                      dispatch({
                                        type: "REMOVE_USER_PROJECT_API",
                                        userProject: {
                                          userId: item.userId,
                                          projectId: record.id,
                                        },
                                      });
                                    }}
                                  >
                                    Delete
                                  </button>
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                      </table>
                    );
                  }}
                >
                  <Avatar src={member.avatar} style={{ marginRight: "20px" }} />
                </Popover>
              );
            })}
            <Popover
              placement="topLeft"
              title={"Add user"}
              content={() => {
                return (
                  <div>
                    {" "}
                    <AutoComplete
                      options={userSearch?.map((user, index) => {
                        return {
                          label: user.name,
                          value: user.userId.toString(),
                        };
                      })}
                      value={value}
                      onChange={(value) => {
                        setValue(value);
                      }}
                      onSelect={(valueSelect, option) => {
                        //set giá trị hộp thoại
                        setValue(option.label);
                        //gọi api gửi về be
                        dispatch({
                          type: "ADD_USER_PROJECT_API",
                          userProject: {
                            projectId: record.id,
                            userId: valueSelect,
                          },
                        });
                      }}
                      placeholder="input here"
                      style={{
                        width: "100%",
                      }}
                      onSearch={(value) => {
                        dispatch({
                          type: "GETUSERAPI",
                          keyword: value,
                        });
                      }}
                    />
                  </div>
                );
              }}
              trigger="click"
            >
              <div className="float-right">
                <Button style={{ borderRadius: "60%" }}>+</Button>
              </div>
            </Popover>
          </div>
        );
      },
    },
    {
      title: "Action",
      key: "Action",
      render: (text, record, index) => {
        return (
          <div>
            <button
              onClick={() => {
                const action = {
                  type: "OPEN_EDIT_FORM_PROJECT",
                  title: "Edit Project",
                  ComponentContentDrawer: <FormEditProject />,
                };
                console.log("ok");
                dispatch(action);
                //dispatch dữ liệu dòng hiện tại lên reducer
                const actionEditProject = {
                  type: "EDIT_PROJECT",
                  projectEditModel: record,
                };
                dispatch(actionEditProject);
              }}
            >
              <EditOutlined />
            </button>
            <Popconfirm
              title="Are you sure to delete this project?"
              onConfirm={() => {
                dispatch({
                  type: "DELETEPROJECTSAGA",
                  idProject: record.id,
                });
              }}
              okText="Yes"
              cancelText="No"
            >
              <button>
                <DeleteOutlined />
              </button>
            </Popconfirm>
          </div>
        );
      },

      // render: (text, record, index) => {
      //   const parse = ReactHtmlParser;
      //   parse(text);
      // },
    },
  ];
  return (
    <div className="container py-5">
      <h3 className="text-4xl py-2">Project Management</h3>
      <Table
        rowKey={"id"}
        columns={columns}
        dataSource={projectList}
        onChange={onChange}
      />
      ;
    </div>
  );
}
